#pragma once

#ifndef M_PI 
#define M_PI 3.14159265359
#endif  
#ifndef M_TAU 
#define M_TAU 6.28318530718
#endif  

#include <GL/glew.h>
#include <HelperGl/Material.h>
#include <HelperGl/PhongShader.h>
#include <HelperGl/Draw.h>
#include <HelperGl/LightServer.h>
#include <SceneGraph/Transform.h>
#include <SceneGraph/Translate.h>
#include <SceneGraph/Rotate.h>
#include <SceneGraph/Scale.h>
#include <SceneGraph/Sphere.h>
#include "SceneGraph/NodeInterface.h"
#include <Math/Matrix4x4f.h>
#include <Math/Quaternion.h>
#include <SceneGraph/CoordinateSystem.h>

namespace SceneGraph
{
	class Insect :
		public Transform
	{
	protected:

		HelperGl::Material m_material_body, 
			m_material_wings, 
			m_material_eyes;

		Rotate * m_rotate_wingr;
		Rotate * m_rotate_wingl;
		Rotate * m_rotate_theta;
		Rotate * m_rotate_phi;
		const Math::Vector3f m_front = Math::makeVector(1, 0, 0);
		const Math::Vector3f m_up = Math::makeVector(0, 1, 0);
		const Math::Vector3f m_right = Math::makeVector(0, 0, 1);
		Translate * m_pos;
		Scale * scale_insect;
		double m_angle;
		double m_count = 0;
		// Nombre de battements d'aile par seconde
		double m_wing_speed;
		// Angle total de battement en radians
		double m_wing_flap;
		// Demi anmplitude (voir draw() )
		double m_wing_flap_div_2;
		double m_size;
		double m_dt = 0;
		GLUquadric * m_quadric;

	public:
		Insect(const HelperGl::Material & material_b,
			const HelperGl::Material & material_w,
			const HelperGl::Material & material_e,
			const Math::Vector3f & position = Math::makeVector(0,0,0),
			double angle = 0.0,
			double wing_speed = 0.0,
			double flap = 0.1,
			double size = 1.
		) :
			m_material_body(material_b),
			m_material_wings(material_w),
			m_material_eyes(material_e),
			m_quadric(gluNewQuadric()),
			m_angle(angle),
			m_wing_speed(wing_speed),
			m_wing_flap(flap)
		{
			m_pos = new Translate(position);
			m_rotate_theta = new Rotate(0, Math::makeVector(0, 1, 0));
			m_rotate_phi = new Rotate(0, Math::makeVector(0, 0, 1));
			m_wing_flap_div_2 = 0.5 * m_wing_flap;
			NodeInterface * body_sphere = new Sphere(material_b);
			NodeInterface * wingr_sphere = new Sphere(m_material_wings);
			NodeInterface * wingl_sphere = new Sphere(m_material_wings);
			NodeInterface * eyel_sphere = new Sphere(m_material_eyes);
			NodeInterface * eyer_sphere = new Sphere(m_material_eyes);
			scale_insect = new Scale(Math::makeVector(1.,1.,1.)*size);
			
			Group * translate_body = new Translate(Math::makeVector(0,0,0));
			Group * scale_body = new Scale(Math::makeVector(3,1,1));
			translate_body->addSon(scale_body);
			scale_body->addSon(body_sphere);

			Group * translate_wingr = new Translate(Math::makeVector(0., 0., 0.5));
			Group * scale_wingr = new Scale(Math::makeVector(1., 0.1, 2.));
			Group * translate_wingr2 = new Translate(Math::makeVector(0, 0, 1));
			m_rotate_wingr = new Rotate(m_angle, Math::makeVector(1, 0, 0));
			translate_wingr->addSon(m_rotate_wingr);
			m_rotate_wingr->addSon(scale_wingr);
			scale_wingr->addSon(translate_wingr2);
			translate_wingr2->addSon(wingr_sphere);

			Group * translate_wingl = new Translate(Math::makeVector(0., 0., -0.5));
			Group * scale_wingl = new Scale(Math::makeVector(1., 0.1, 2.));
			Group * translate_wingl2 = new Translate(Math::makeVector(0, 0, -1));
			m_rotate_wingl = new Rotate(m_angle, Math::makeVector(1, 0, 0));
			translate_wingl->addSon(m_rotate_wingl);
			m_rotate_wingl->addSon(scale_wingl);
			scale_wingl->addSon(translate_wingl2);
			translate_wingl2->addSon(wingl_sphere);

			Group * translate_wings = new Translate(Math::makeVector(0., 0.2, 0.));
			translate_wings->addSon(translate_wingl);
			translate_wings->addSon(translate_wingr);

			double depth_eyes = 2.0;
			double width_eyes = 0.5;
			double size_eyes = 1.5;

			Group * translate_eyes = new Translate(Math::makeVector(depth_eyes, 0.2, 0.));
			Group * translate_eyel = new Translate(Math::makeVector(0., 0., -width_eyes));
			Group * translate_eyer = new Translate(Math::makeVector(0., 0., width_eyes));
			Math::Vector3f scaling = Math::makeVector(0.5, 0.5, 0.5)*size_eyes;
			Group * scale_eyel = new Scale(scaling);
			Group * scale_eyer = new Scale(scaling);
			translate_eyel->addSon(scale_eyel);
			translate_eyer->addSon(scale_eyer);
			scale_eyel->addSon(eyel_sphere);
			scale_eyer->addSon(eyer_sphere);
			translate_eyes->addSon(translate_eyer);
			translate_eyes->addSon(translate_eyel);
			scale_insect->addSon(translate_body);
			scale_insect->addSon(translate_eyes);
			scale_insect->addSon(translate_wings);
			m_rotate_theta->addSon(m_rotate_phi);
			m_rotate_phi->addSon(scale_insect);
			//m_rotate_phi->addSon(new CoordinateSystem(0.5));
			m_pos->addSon(m_rotate_theta);
			addSon(m_pos);
			//m_rotate_theta->addSon(new CoordinateSystem(0.5));
			//m_sons.push_back(translate_wingl);

			gluQuadricNormals(m_quadric, GLU_SMOOTH);
			if (m_material_body.hasTexture() || m_material_wings.hasTexture() || m_material_eyes.hasTexture())
			{
				gluQuadricTexture(m_quadric, true);
			}
		}

		~Insect() 
		{
			gluDeleteQuadric(m_quadric);
		}

		void setWingSpeed(double wing_speed)
		{
			m_wing_speed = wing_speed;
		}

		void setWingFlap(double flap)
		{
			m_wing_flap = flap;
			m_wing_flap_div_2 = 0.5 * m_wing_flap;
		}

		void setAngle(double angle)
		{
			m_angle = angle;
			m_rotate_wingl->setAngle(m_angle);
			m_rotate_wingr->setAngle(-m_angle);
			m_count = acos(m_angle / (M_PI*m_wing_flap)); // pour des transitions douces
		}

		

		void set_dt(double dt)
		{
			m_dt = dt;
		}

		void setOrientation(const Math::Vector3f & target)
		{
			Math::Vector3f target_n = target.normalized();
			m_rotate_theta->setAngle(0.0);
			m_rotate_phi->setAngle(0.0);

			// Passage en coordonées sphériques

			// Rotation autour de l'axe Y
			double theta;
			if (fabs(target_n[0]) < 0.00001) {
				theta = target_n[2] < 0 ? M_PI / 2 : -M_PI / 2;
			}
			else {
				theta = -atan2(target_n[2], target_n[0]);
			}

			// Rotation autour de l'axe Z
			double phi = M_PI / 2 - acos(target_n[1]);
			
			m_rotate_theta->setAngle(theta);
			m_rotate_phi->setAngle(phi);
		}

		void setPosition(const Math::Vector3f & translation)
		{
			m_pos->setTranslation(translation);
		}

		virtual void draw()
		{	
			m_count += m_dt * m_wing_speed;
			if (m_count > 1)
				m_count = 0;

			m_angle = m_wing_flap_div_2 * cos(m_count * M_TAU); // 0.5* flap car  -1 < cos < 1 donc il faut diviser l'amplitude par deux

			m_rotate_wingl->setAngle(m_angle);
			m_rotate_wingr->setAngle(-m_angle);
			
			Group::draw();
		}
	};
}

