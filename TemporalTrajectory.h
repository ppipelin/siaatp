#pragma once
#include "Math/Vectorf.h"
#include "Math/Vector.h"
#include "Math/Matrix4x4f.h"
#include <vector>
#include <tuple>
#include <limits>
#include <math.h>
#include <HermiteSplines.h>

class TemporalTrajectory
{
	std::vector<Math::Vector3f> m_pos;
	std::vector<HermiteSplines> m_splines;

public:
	bool start_is_end;
	TemporalTrajectory(const std::vector<Math::Vector3f>  & positions, bool start_is_end = false):
		m_pos(positions),
		start_is_end(start_is_end)
	{	
		update();
	}

	void setPositions(const std::vector<Math::Vector3f>  & positions) {
		m_pos = positions;
		update();
	}

	void normalize() {
		float max = 0;
		for (const Math::Vector3f & pos : m_pos) {
			for (int d = 0; d < 3; ++d) if (fabs(pos[d]) > max) max = fabs(pos[d]);
		}
		for (Math::Vector3f & pos : m_pos) {
			pos = pos / max;
		}
		update();
	}

	void scale(const Math::Vector3f & s) {
		for (Math::Vector3f & pos : m_pos) {
			for (int d = 0; d < 3; ++d) pos[d] = pos[d] * s[d];
		}
		update();
	}

	void scale(float s) {
		for (Math::Vector3f & pos : m_pos) {
			pos = pos * s;
		}
		update();
	}

	~TemporalTrajectory()
	{}

	Math::Vector3f compute(double time, bool splines = true) const
	{
		// Time est suppos� etre entre le premier point et le dernier
		int i = floor(time);
		float u = time - i;
		return splines ?
			m_splines[i].compute(u) :
			m_pos[i] * (1 - u) + m_pos[i + 1] * u;

	}

	int duration() {
		return m_pos.size() - 1;
	}

private:
	void update() {
		Math::Vector3f D0, D1, P0, P1, Dstart;
		m_splines.clear();
		for (int i = 0; i < m_pos.size() - 1; ++i)
		{
			P0 = m_pos[i];
			P1 = m_pos[i + 1];
			
			if (i == 0)
			{
				D0 = P1 - P0;
				Dstart = D0;
			}
			else
			{
				D0 = D1;
			}

			if (i <= m_pos.size() - 3)
			{
				D1 = m_pos[i + 2] - P0;
			}
			else
			{
				if (start_is_end)
				{
					D1 = Dstart;
				}
				else
				{
					D1 = P1 - P0;
				}
			}

			HermiteSplines H = HermiteSplines(P0, P1, D0, D1);
			m_splines.push_back(H);
		}
	}
};