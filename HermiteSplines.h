#pragma once
#include "Math/Vectorf.h"
#include "Math/Vector.h"
#include "Math/Matrix4x4f.h"
#include <vector>

class HermiteSplines
{
private:

	Math::Vector3f P0, P1, D0, D1;

public:

	HermiteSplines(
		const Math::Vector3f & P0_,
		const Math::Vector3f & P1_,
		const Math::Vector3f & D0_,
		const Math::Vector3f & D1_
	):
	P0(P0_),
	P1(P1_),
	D0(D0_),
	D1(D1_)
	{}


	Math::Vector3f compute(double u) const
	{
		double u2 = u * u;
		double u3 = u2 * u;
		Math::Vector3f a = P0 * 2 + P1 * (-2) + D0 + D1;
		Math::Vector3f b = P0 * (-3) + P1 * 3 + D0 * (-2) + D1 * (-1);
		return  a * u3 + b * u2 + D0 * u + P0;
	}
};

