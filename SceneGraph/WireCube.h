#ifndef _SceneGraph_WireCube_H
#define _SceneGraph_WireCube_H

#include <GL/glew.h>
#include <SceneGraph/NodeInterface.h>
#include <HelperGl/Color.h>

namespace SceneGraph
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// \class	WireCube
	///
	/// \brief	Draws the current coordinate system. X: red, Y: green, Z: blue
	///
	/// \author	F. Lamarche, Université de Rennes 1
	/// \date	06/03/2016
	////////////////////////////////////////////////////////////////////////////////////////////////////
	class WireCube : public NodeInterface
	{
	protected:
		float m_length;
		float r, g, b;


	public:

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	WireCube::WireCube(float length)
		///
		/// \brief	Constructor.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	06/03/2016
		///
		/// \param	length	The length of each axis.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		WireCube(float length = 1.0f, float r_ = 0.0, float g_ = 0.0, float b_ = 0.0)
			: m_length(length), r(r_), g(g_), b(b_)
		{}

		virtual void draw()
		{
			glPushAttrib(GL_ENABLE_BIT);
			glDisable(GL_LIGHTING);
			//y
			glColor3f(r, g, b);
			glBegin(GL_LINES);
			glVertex3f(-m_length, -m_length, -m_length);
			glVertex3f(-m_length, m_length, -m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(m_length, -m_length, -m_length);
			glVertex3f(m_length, m_length, -m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-m_length, -m_length, m_length);
			glVertex3f(-m_length, m_length, m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(m_length, -m_length, m_length);
			glVertex3f(m_length, m_length, m_length);
			glEnd();
			//x
			glBegin(GL_LINES);
			glVertex3f(m_length, -m_length, -m_length);
			glVertex3f(-m_length, -m_length, -m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(m_length, -m_length, m_length);
			glVertex3f(-m_length, -m_length, m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(m_length, m_length, -m_length);
			glVertex3f(-m_length, m_length, -m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(m_length, m_length, m_length);
			glVertex3f(-m_length, m_length, m_length);
			glEnd();
			//z
			glBegin(GL_LINES);
			glVertex3f(m_length, m_length, m_length);
			glVertex3f(m_length, m_length, -m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(m_length, -m_length, m_length);
			glVertex3f(m_length, -m_length, -m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-m_length, -m_length, m_length);
			glVertex3f(-m_length, -m_length, -m_length);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-m_length, m_length, m_length);
			glVertex3f(-m_length, m_length, -m_length);
			glEnd();

			glPopAttrib();
		}
	};
}

#endif