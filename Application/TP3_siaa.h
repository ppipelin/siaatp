#ifndef _Application_TP3_siaa_H
#define _Application_TP3_siaa_H

#include <HelperGl/Camera.h>
#include <HelperGl/LightServer.h>
#include <HelperGl/Color.h>
#include <Application/BaseWithKeyboard.h>
#include <Application/KeyboardStatus.h>
#include <SceneGraph/NodeInterface.h>
#include <SceneGraph/Group.h>
#include <SceneGraph/Sphere.h>
#include <SceneGraph/MeshVBO_v2.h>
#include <SceneGraph/Cylinder.h>
#include <SceneGraph/Insect.h>
#include <SceneGraph/Translate.h>
#include <SceneGraph/Rotate.h>
#include <SceneGraph/Scale.h>
#include <SceneGraph/Insect.h>
#include <SceneGraph/Patch.h>
#include <GL/compatibility.h>
#include <HermiteSplines.h>
#include <TemporalTrajectory.h>
#include <Animation/SpringMassSystem.h>
#include <Animation/PonctualMass.h>
#include <Animation/Particle.h>
#include <Animation/ParticleSystem.h>

// Aliases pour gagner du temps
typedef HelperGl::Color Color;
typedef SceneGraph::Patch Patch;
typedef Animation::SpringMassSystem  SpringMassSystem;
typedef Animation::SpringMassSystem::Link Link;
typedef Animation::SpringMassSystem::Mass Mass;
typedef Animation::SpringMassSystem::PatchDescriptor PatchDescriptor;
typedef Animation::Particle Particle;
typedef Animation::ParticleSystem ParticleSystem;
typedef Animation::PonctualMass PonctualMass;


namespace Application
{

	class TP3_siaa : public BaseWithKeyboard
	{
	protected:
		HelperGl::Camera m_camera;
		SceneGraph::Group m_root;

		float width = 3.0f;
		float height = 3.0f;
		int width_nb = 60;
		int height_nb = 60;
		int extent = 2;
		float total_mass = 10;

		SpringMassSystem system;
		Patch * patch;
		Translate * trans_sphere;
		PatchDescriptor pd;

		float gravity_coefficient = 9.81f;
		float viscosity_coefficient = 0.005f;
		float stiffness_coefficient = 100.0f; //10000.0f;
		float max_speed = 200.0f;	
		float ground_height = -2.5f;
		Vector3f ground_center = Math::makeVector(0.0f, ground_height, 0.0f);
		float ground_size = 3.0f;
		float ground_friction = 0.05f;
		float sphere_friction = 0.01f;
		Vector3f gravity_vector = Math::makeVector(0.0f, -gravity_coefficient, 0.0f);
		Vector3f sphere_center = Math::makeVector(0.0f, -2.0f, 0.0f);
		float sphere_radius = 0.5;
		bool euler = 1;  // False => verlet
		HelperGl::Material mat_euler, mat_verlet;
		std::function<Vector3f (const Mass &)> gravity = 
			[this](const Mass & mass) -> Vector3f
		{
			return gravity_vector * mass.m_mass;
		};
		std::function<Vector3f(const Mass &)> viscosity =
			[this](const Mass & mass) -> Vector3f
		{
			return - mass.m_speed * viscosity_coefficient;
		};
		std::function<Vector3f (const Mass &, const Mass &, const Link &)> stiffness = 
			[this](const Mass & m1, const Mass & m2 , const Link & link) -> Vector3f
		{
			Vector3f m1tom2 = (m2.m_position - m1.m_position);
			float dist = m1tom2.norm();
			Vector3f dir = m1tom2 / dist;
			float stretch = dist - link.m_initialLength;
			return dir * stretch * stiffness_coefficient;
		};
		std::function<std::pair<Vector3f, Vector3f> (const Mass &, const Mass &, float)> integrator_euler =
			[this](const Mass & previous, const Mass & current, float dt) -> std::pair<Vector3f,Vector3f>
		{
			Vector3f speed = current.m_speed;
			Vector3f position = current.m_position;
			if (!current.m_isConstrained) {
				// Update acc -> speed -> pos
				Vector3f a = current.m_forces / current.m_mass;
				speed = speed + a * dt;
				float speed_norm = speed.norm();
				if (speed_norm > max_speed) {
					speed = (speed / speed_norm) * max_speed;
				}
				position = position + speed * dt;
			}
			return std::make_pair(position, speed);
		};
		// On doit utiliser une variante de l'algorithme de verlet car la viscosit� est une force qui d�pend de la vitesse.
		// Pour l'obtenir avec l'algo classique, on doit faire des erreurs d'approximation.
		// L'algo de verlet � un pas utilise la vitesse et permet donc de l'avoir de mani�re assez pr�cise :
		std::function<std::pair<Vector3f, Vector3f>(const Mass &, const Mass &, float)> integrator_verlet =
			[this](const Mass & previous, const Mass & current, float dt) -> std::pair<Vector3f, Vector3f>
		{
			Vector3f speed = current.m_speed; // assez instable si on met previous ... ?
			Vector3f position = previous.m_position; // Marche pas si on met current ... ?
			if (!current.m_isConstrained) {
				current.m_acceleration = current.m_forces / current.m_mass;
				speed = speed + (current.m_acceleration + previous.m_acceleration)*(dt * 0.5);
				float speed_norm = speed.norm();
				if (speed_norm > max_speed) {
					speed = (speed / speed_norm) * max_speed;
				}
				position = position + speed * dt + previous.m_acceleration*(dt * dt * 0.5);
			}
			return std::make_pair(position, speed);
			// Bon, en fait ca devrait pas marcher parceque j'ai pas suivi exactement l'algo (pas coh�rent previous/current et ordre des lignes) :
			// https://femto-physique.fr/omp/methode-de-verlet.php
			// Mais bon ca marche quand m�me et c'est l�gerement plus stable que euler (moins de mini oscillations)
		};
		std::function<std::pair<Vector3f, Vector3f>(const Mass &, const Mass &)> ground =
			[this](const Mass & previous, const Mass & current) -> std::pair<Vector3f, Vector3f>
		{
			Vector3f speed = current.m_speed;
			Vector3f position = current.m_position;
			Vector3f posprim = position; posprim[1] = ground_height;
			float d = (position-ground_center).norm();
			if (position[1] <= ground_height && position[1] >= ground_height - 0.3f && d < ground_size) {
				position[1] = ground_height;
				speed[1] = 0;
				speed = speed * (1 - ground_friction); // Friction
			}
			return std::make_pair(position, speed);
		};
		std::function<std::pair<Vector3f, Vector3f>(const Mass &, const Mass &)> sphere =
			[this](const Mass & previous, const Mass & current) -> std::pair<Vector3f, Vector3f>
		{
			Vector3f speed = current.m_speed;
			Vector3f position = current.m_position;
			Vector3f postocenter = trans_sphere->getTranslation() - position;
			float dist = postocenter.norm();
			Vector3f dir = postocenter / dist;
			if (dist <= sphere_radius) {
				float delta = sphere_radius - dist;
				position = position + dir * (-delta);
				Vector3f speedn = speed.normalized();
				float f = dir * speedn; // cos de l'angle form�
				if (f > 0) { // Si la vitesse de la masse est dirig�e vers le demi espace contenant le centre de la sphere
					speed = (speedn - (dir * f))* speed.norm(); // Projection de v(t) sur la tangente � la sphere au point d'impact
					speed = speed * sin(acos(f)); // On consid�re que la sphere absorbe tout le choc: v = 0 si l'impact est frontal, et v presque inchang�e si l'impact est tangent
					speed = speed * (1 - sphere_friction); // Friction
				}
			}
			return std::make_pair(position, speed);
		};
		double last_input_time = 10;
		bool input = false;
		Math::Matrix4f rotation = Math::Matrix4f(
			Math::makeVector(1.0f, 0.0f, 0.0f, 0.0f),
			Math::makeVector(0.0f, 0.0f, 1.0f, 0.0f),
			Math::makeVector(0.0f, -1.0f, 0.0f, 0.0f),
			Math::makeVector(0.0f, 0.0f, 0.0f, 1.0f)
		);
		Math::Matrix4f translation = Math::Matrix4f(
			Math::makeVector(1.0f, 0.0f, 0.0f, -width/2),
			Math::makeVector(0.0f, 1.0f, 0.0f, -height/2),
			Math::makeVector(0.0f, 0.0f, 1.0f, 0.0f),
			Math::makeVector(0.0f, 0.0f, 0.0f, 1.0f)
		);
		Math::Matrix4f transformation = rotation * translation; // D'abord la translation, puis la rotation
		virtual void handleKeys()
		{
			// The camera translation speed
			float cameraSpeed = 5.0f;
			// The camera rotation speed (currently not used)
			float rotationSpeed = float(Math::pi / 2.0);

			// quit
			//if(m_keyboard.isPressed('q')) { quit() ; } // Non
			// Go front
			if (m_keyboard.isPressed('z')) { m_camera.translateFront(-cameraSpeed * (float)getDt()); }
			// Go back
			else if (m_keyboard.isPressed('s')) { m_camera.translateFront(cameraSpeed * (float)getDt()); }
			// Go left
			else if (m_keyboard.isPressed('q')) { m_camera.translateRight(-cameraSpeed * (float)getDt()); }
			// Go right
			else if (m_keyboard.isPressed('d')) { m_camera.translateRight(cameraSpeed * (float)getDt()); }
			// Go down

			if (m_keyboard.isPressed('w')) { m_camera.translateUp(-cameraSpeed * (float)getDt()); }
			// Go up
			else if (m_keyboard.isPressed(VK_SPACE)) { m_camera.translateUp(cameraSpeed * (float)getDt()); }

			if (m_keyboard.isPressed('5')) { m_camera.rotateUp(rotationSpeed* (float)getDt()); }
			else if (m_keyboard.isPressed('2')) { m_camera.rotateUp(-rotationSpeed * (float)getDt()); }
			else if (m_keyboard.isPressed('3')) { m_camera.rotateRight(rotationSpeed * (float)getDt()); }
			else if (m_keyboard.isPressed('1')) { m_camera.rotateRight(-rotationSpeed * (float)getDt()); }
			if (last_input_time > 0.3) { // 0.3s avant prochain input...
				input = false;
				if (m_keyboard.isPressed('p')) {
					input = true;
					last_input_time = 0.0;
					std::vector<int> ids;
					int cpt = 0;
					bool found = false;
					for (const Mass & m : system.getMasses()) {
						if (m.m_isConstrained) {
							ids.push_back(m.m_id);
							found |= true;
						}
					}
					if (found) {
						int index = floor(ids.size()*double(rand()) / RAND_MAX);
						std::cout << "Masse relachee (id : " << ids[index] << ")" << std::endl;
						system.unconstrainPosition(ids[index]);
					}
				}
				if (m_keyboard.isPressed('r')) {
					input = true;
					last_input_time = 0.0;
					resetPatch();
				}
				if (m_keyboard.isPressed('v')) {
					input = true;
					last_input_time = 0.0;
					euler = !euler;
					system.setIntegrator(euler ? integrator_euler : integrator_verlet);
					patch->set_material(euler ? mat_euler : mat_verlet);
					std::cout <<( euler ? "Euler" : "Verlet") << std::endl;
				}
			}
			if (last_input_time > 0.1) { // 0.1s avant prochain input...
				input = false;
				if (m_keyboard.isPressed('u')) {
					input = true;
					last_input_time = 0.0;
					stiffness_coefficient *= 1.1;
					std::cout << "Raideur augment�e (" << stiffness_coefficient <<")"<< std::endl;
				}
				if (m_keyboard.isPressed('i')) {
					input = true;
					last_input_time = 0.0;
					stiffness_coefficient *= 1.0 / 1.1;
					std::cout << "Raideur diminu�e (" << stiffness_coefficient << ")" << std::endl;
				}
				if (m_keyboard.isPressed('j')) {
					input = true;
					last_input_time = 0.0;
					viscosity_coefficient *= 1.1;
					std::cout << "Viscosite augment�e (" << viscosity_coefficient << ")" << std::endl;
				}
				if (m_keyboard.isPressed('k')) {
					input = true;
					last_input_time = 0.0;
					viscosity_coefficient *= 1.0 / 1.1;
					std::cout << "Viscosite diminu�e (" << viscosity_coefficient << ")" << std::endl;
				}
			}
		}

	public:
		TP3_siaa()
		{
			std::cout << "############################################## " << std::endl << std::endl;;
			std::cout << "Interactions clavier : " << std::endl;
			std::cout << " r : Reset tout le tissu en centre (0,0,0)" << std::endl;
			std::cout << " p : Detacher un point d'ancrage au hasard (au depart, un point d'ancrage par coin)" << std::endl;
			std::cout << " v : Changer de methode d'integration (euler <-> verlet) (euler par defaut)" << std::endl;
			std::cout << " u / i : augmenter / diminuer le coeff de raideur" << std::endl;
			std::cout << " j / k : augmenter / diminuer le coeff de viscosite" << std::endl;
			std::cout << std::endl << "############################################## " << std::endl << std::endl;;
		}

		void updatePatch() { system.copyMassesPositionsTo(patch->getVertices().begin()); }

		void resetPatch() {
			system.reset(transformation);
			updatePatch();
		}

		virtual void initializeRendering()
		{
			drawFPS(true);

			Math::Matrix4x4f M = Math::Matrix4x4f(
				Math::makeVector(0.643976, -0.403795, 0.649807, 0.0),
				Math::makeVector(0.0, 0.84937, 0.527804, 0.0),
				Math::makeVector(-0.765046, -0.339893, 0.546973, 0.0),
				Math::makeVector(0.0, 0.0, 0.0, 1.0)
			);
			Vector3f v = Math::makeVector(3.65106, 0.648891, 3.06526);
			m_camera.setOrientation(M);
			m_camera.setPosition(v);

			Color lightColor(1.0, 1.0, 1.0);
			Color lightAmbiant(0.5, 0.5, 0.5, 0.0);
			Math::Vector4f lightPosition = Math::makeVector(0.0f, 0.0f, 10000.0f, 1.0f); // Point light centered in 0,0,0
			HelperGl::LightServer::Light * light = HelperGl::LightServer::getSingleton()->createLight(lightPosition.popBack(), lightColor, lightColor, lightColor);
			HelperGl::Material mat1, ground_mat, sphere_mat;
			mat1.setAmbiant(Color(1, 1, 0));
			mat_euler.setDiffuse(Color(1, 0.3, 0.3));
			mat_verlet.setDiffuse(Color(0.3, 0.3, 1));
			Color ground_col = Color(1.0, 1.0, 1.0);
			Color sphere_col = Color(0.0, 0.0, 1.0);
			ground_mat.setDiffuse(ground_col);
			sphere_mat.setDiffuse(sphere_col);
			light->enable();
			light->setAmbient(lightAmbiant);
			m_root.addSon(new SceneGraph::CoordinateSystem());
		
			pd = system.createPatch(width, width_nb-1, height, height_nb-1, extent, total_mass, transformation);
			
			pd.constrainPosition(0, 0);
			pd.constrainPosition(0, height_nb - 1);
			pd.constrainPosition(width_nb - 1, height_nb - 1);
			pd.constrainPosition(width_nb - 1, 0);

			//for (int w = 0; w < width_nb ; ++w) {
			//	for (int h = 0; h < height_nb; ++h) {
			//		if (w == 0 || w == width_nb - 1 || h  == 0 || h == height_nb - 1)
			//			if(double(rand())/RAND_MAX < 0.1)
			//				pd.constrainPosition(w, h);
			//	}
			//}


			patch = new Patch(width_nb, height_nb, euler ? mat_euler : mat_verlet);
	
			updatePatch();
			Translate * trans = new Translate(Math::makeVector(-width / 2, -height/2, 0.0f));
			Rotate * rot = new Rotate(-M_PI/2, Math::makeVector(1, 0, 0));
			rot->addSon(trans);
			trans->addSon(patch);
			m_root.addSon(patch);

			Cylinder * cylinder = new Cylinder(ground_mat, ground_size, ground_size, 0.3f, 100);
			Rotate * rot_cyl = new Rotate(M_PI / 2, Math::makeVector(1, 0, 0));
			Translate * trans_cyl = new Translate(Math::makeVector(0.0f, ground_height-0.03f, 0.0f));
			trans_cyl->addSon(rot_cyl);
			rot_cyl->addSon(cylinder);
			m_root.addSon(trans_cyl);
			Sphere * sphere_object = new Sphere(sphere_mat, 1.0f, 50, 50);
			Scale * scale_sphere = new Scale(Math::makeVector(1.0f, 1.0f, 1.0f)*(sphere_radius-0.03f));
			trans_sphere = new Translate(sphere_center);
			trans_sphere->addSon(scale_sphere);
			scale_sphere->addSon(sphere_object);
			m_root.addSon(trans_sphere);
			system.addForceFunction(gravity, true);
			system.addForceFunction(viscosity, true);
			system.addLinkForceFunction(stiffness, true);
			system.setIntegrator(euler ? integrator_euler : integrator_verlet, true);
			system.addPositionConstraint(ground, true);
			system.addPositionConstraint(sphere, true);
			double frequence = 1000;
			double period = 1.0 / frequence;
			system.setInternalPeriod(period);

		}

		void background() {
			glClearColor(0.8, 0.8, 0.8, 0.0);
		}

		Vector3f shiftx = Math::makeVector(0, 0, 0);
		Vector3f shifty = Math::makeVector(0, 0, 0);
		Vector3f shiftz = Math::makeVector(0, 0, 0);
		float rad = 0.2;
		float time = 0;
		virtual void render(double dt)
		{
			last_input_time += dt;
			time += dt;
			background();
			handleKeys();
			GL::loadMatrix(m_camera.getInverseTransform());
			shiftx[0] = rad * cos(time);
			shifty[1] = 2 * rad * abs(sin(time));
			shiftz[2] = rad * sin(time);
			trans_sphere->setTranslation(
				sphere_center
				+ shiftx
				+ shifty
				+ shiftz);
			system.update(dt);
			updatePatch();
			m_root.draw();
		}
	};
}

#endif