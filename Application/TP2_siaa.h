#ifndef _Application_TP2_siaa_H
#define _Application_TP2_siaa_H

#include <HelperGl/Camera.h>
#include <HelperGl/LightServer.h>
#include <HelperGl/Color.h>
#include <Application/BaseWithKeyboard.h>
#include <Application/KeyboardStatus.h>
#include <SceneGraph/NodeInterface.h>
#include <SceneGraph/WireCube.h>
#include <SceneGraph/Group.h>
#include <SceneGraph/Sphere.h>
#include <SceneGraph/MeshVBO_v2.h>
#include <SceneGraph/Cylinder.h>
#include <SceneGraph/Insect.h>
#include <SceneGraph/Translate.h>
#include <SceneGraph/Rotate.h>
#include <SceneGraph/Scale.h>
#include <SceneGraph/Insect.h>
#include <GL/compatibility.h>
#include <HermiteSplines.h>
#include <TemporalTrajectory.h>
#include <Animation/KinematicChain.h>
#include <CCD.h>
#include <random>
#include <TemporalTrajectory.h>
#include <HermiteSplines.h>

typedef Animation::KinematicChain  KChain;
typedef Animation::KinematicChain::Node KNode;
typedef Animation::KinematicChain::DegreeOfFreedom  DOF;
typedef SceneGraph::Rotate  Rotate;
typedef SceneGraph::Translate  Translate;
typedef SceneGraph::Scale  Scale;
typedef SceneGraph::Group  Group;
typedef SceneGraph::Sphere  Sphere;
typedef SceneGraph::Cylinder  Cylinder;
typedef Math::Vector3f  Vector3f;

namespace Application
{


	class TP2_siaa : public BaseWithKeyboard
	{

	protected:
		

		HelperGl::Camera m_camera;
		Group m_root;

		std::vector<Rotate *> rotates;
		std::vector<Translate *> translates;
		Translate target = Translate(Math::makeVector(0, 0, 0));
		Scale *  target_sphere;
		SceneGraph::Insect * target_insect;
		Vector3f last_pos;
		Translate target_previous = Translate(Math::makeVector(0, 0, 0));
		std::vector<DOF> dofs;
		KChain kchain;
		KNode * extremity;
		CCD * ccd;
		Vector3f dir;

		float chain_length = 6.0f;
		int chain_size = 10;
		float factor = 0.7;
		float segment_length = (chain_length/float(chain_size));
		float segment_width = 0.1*factor;
		float articulation_size = 0.2*factor;
		float angle_width = M_PI;
		
		float attenuation = 0.05;
		float max_variation = 0.1;
		float range = 0.005;
		float eps = 0.0001;

		Vector3f D0 = Math::makeVector(0, 20, 0);
		Vector3f D1 = Math::makeVector(0, -20, 0);
		Vector3f P0 = Math::makeVector(-1.0, 0.0, 0.0)*chain_length;
		Vector3f P1 = Math::makeVector(1.0, 0.0, 0.0)*chain_length;

		HermiteSplines H = HermiteSplines(P0, P1, D0, D1);

		std::vector<Math::Vector3f> positions = {
			Math::makeVector(0.0,0.0,0.),
			Math::makeVector(3.,1.,1.),
			Math::makeVector(1.,1.,-1.),
			Math::makeVector(-1.,1.,-1.),
			Math::makeVector(-2.,0.0,1.0),
			Math::makeVector(0.0,4.0,4.0),
			Math::makeVector(2.0,1.0,2.0),
			Math::makeVector(1.0,1.0,2.0),
			Math::makeVector(-4.0,3.0,1.0),
			Math::makeVector(-2.0,1.0,-4.0),
			Math::makeVector(0.0,1.0,-2.0),
			Math::makeVector(0.0,0.0,0.0)
		};

		TemporalTrajectory T = TemporalTrajectory(positions, true);

		double u = 0;
		double max_time = T.duration();
		double max_time_interpolation = 2;
		bool forward = true;
		double time = 0, time_interpolation = 0;
		bool reset = false;
		int state = 0;
		std::vector<float> previous, next, interpoled;
		double last_input_time = 10;
		bool input = false;


		Group traj_tt = Group();

		virtual void handleKeys()
		{
			// The camera translation speed
			float cameraSpeed = 5.0f;
			// The camera rotation speed (currently not used)
			float rotationSpeed = float(Math::pi / 2.0);
			//std::cout << m_camera.getTransform() << std::endl;
			//std::cout << std::endl;

			// quit
			//if(m_keyboard.isPressed('q')) { quit() ; }
			// Go front

			if (m_keyboard.isPressed('z')) { m_camera.translateFront(-cameraSpeed * (float)getDt()); }
			// Go back
			else if (m_keyboard.isPressed('s')) { m_camera.translateFront(cameraSpeed * (float)getDt()); }
			// Go left
			else if (m_keyboard.isPressed('q')) { m_camera.translateRight(-cameraSpeed * (float)getDt()); }
			// Go right
			else if (m_keyboard.isPressed('d')) { m_camera.translateRight(cameraSpeed * (float)getDt()); }
			// Go down

			if (m_keyboard.isPressed('w')) { m_camera.translateUp(-cameraSpeed * (float)getDt()); }
			// Go up
			else if (m_keyboard.isPressed(VK_SPACE)) { m_camera.translateUp(cameraSpeed * (float)getDt()); }

			if (m_keyboard.isPressed('5')) { m_camera.rotateUp(rotationSpeed* (float)getDt()); }
			else if (m_keyboard.isPressed('2')) { m_camera.rotateUp(-rotationSpeed * (float)getDt()); }
			else if (m_keyboard.isPressed('3')) { m_camera.rotateRight(rotationSpeed * (float)getDt()); }
			else if (m_keyboard.isPressed('1')) { m_camera.rotateRight(-rotationSpeed * (float)getDt()); }
			if (last_input_time > 0.8) { // 0.8s avant prochain input...
				input = false;
				if (m_keyboard.isPressed('n')) {
					resetChain();
					target_previous.setTranslation(target.getTranslation());
					Vector3f new_pos = randomPointSphere(segment_length*chain_size,true);
					target.setTranslation(new_pos);
					if(state == 2) computeInterpolation();
					last_input_time = 0;
					input = true;
					std::cout << " Nouvelle position aleatoire de la cible (" << new_pos <<")"<< std::endl;
				}
				if (m_keyboard.isPressed('r')) {
					resetChain();
					last_input_time = 0;
					input = true;
					std::cout << " Reset de la chaine" << std::endl;
				}
				if (m_keyboard.isPressed('h')) {
					resetChain();
					state = (state + 1) % 3;
					if (state == 0) {
						m_root.addSon(&traj_tt);
						m_root.removeSon(&target_previous);
						target.removeSon(target_sphere);
						target.addSon(target_insect);
						std::cout << " 0 -> suivre une hermitespline initialisee" << std::endl;
					}
					else if(state == 1){
						m_root.removeSon(&traj_tt);
						dir = speed.normalized();
						std::cout << " 1 -> suivre les rebonds de la cible contre un cube" << std::endl;
					}
					else if (state == 2) {
						target.addSon(target_sphere);
						target.removeSon(target_insect);
						computeInterpolation();
						m_root.addSon(&target_previous);
						std::cout << " 2 -> faire l'interpolation entre la position courante et la nouvelle position" << std::endl;
					}
					last_input_time = 0;
					input = true;
				}
				if (m_keyboard.isPressed('p')) {
					dir = randomPointSphere();
					last_input_time = 0;
					input = true;
					std::cout << " Nouvelle direction aleatoire de la cible qui rebondit ("<<dir<<")"<< std::endl;
				}
			}
		}

	public:
		TP2_siaa()
		{
			std::cout << "############################################## " << std::endl << std::endl;;
			std::cout << " Interactions clavier : " << std::endl;
			std::cout << " r : reset les dofs de la chaine" << std::endl;
			std::cout << " n : nouvelle cible aleatoire" << std::endl;
			std::cout << " h : action a realiser  =0, =1 ou =2 (incrementation)" << std::endl;
			std::cout << "       0 (fond rouge) -> suivre une hermitespline initialisee" << std::endl;
			std::cout << "       1 (fond vert)  -> suivre les rebonds de la cible contre un cube" << std::endl;
			std::cout << "       2 (fond bleu)  -> faire l'interpolation entre la position courante et la nouvelle position" << std::endl;
			std::cout << " p : nouvelle direction aleatoire pour la cible (quand action = 1 (rebond))" << std::endl;

			std::cout << std::endl << "############################################## " << std::endl << std::endl;;

		}

		void updateChain()
		{
			for (int i = 0 ; i < rotates.size() ; ++i)
			{
				rotates[i]->setAngle(dofs[i]);
			}
		}

		void updateChain(const std::vector<float> & dofs_floats)
		{
			for (int i = 0; i < dofs.size(); ++i)
			{
				dofs[i] = dofs_floats[i];
			}
			updateChain();
		}

		void resetChain() {
			for (int i = 0; i < dofs.size(); ++i) {
				dofs[i] = 0;
			}
			updateChain();
		}


		void addChain(int chain_size = 3, float segment_length = 1.0f, float segment_width = 0.1, float articulation_size = 0.2) {
			HelperGl::Material mat_segment, mat_articulation_x, mat_articulation_z;
			mat_segment.setDiffuse(HelperGl::Color(0, 1, 0));
			mat_articulation_x.setDiffuse(HelperGl::Color(0, 0, 1));
			mat_articulation_z.setDiffuse(HelperGl::Color(1, 0, 0));
			KNode * current = kchain.getRoot();
			
			for (int i = 0 ; i < chain_size ; ++i)
			{
				double a = 0;
				//double a = 2 * M_PI*(double(rand()) / RAND_MAX - 0.5);
				Vector3f axis_x = Math::makeVector(1, 0, 0);
				Vector3f axis_z = Math::makeVector(0, 0, 1);
				rotates.push_back(new Rotate(a, axis_x));
				rotates.push_back(new Rotate(a, axis_z));
				translates.push_back(new Translate(Math::makeVector(0.0,1.0,0.0)*segment_length));
				double span = (i >= 2) ? angle_width : 100000;
				current = kchain.addDynamicRotation(current, axis_x, Math::makeInterval(float(-span / 2), float(span / 2)), 0.0f);
				dofs.push_back(current->getDOF()[0]);
				current = kchain.addDynamicRotation(current, axis_z, Math::makeInterval(float(-span / 2), float(span / 2)), 0.0f);
				dofs.push_back(current->getDOF()[0]);
				current = kchain.addStaticTranslation(current, Math::makeVector(0.0, 1.0, 0.0)*segment_length);
			}
			extremity = current;
			for (int i = 0 ; i < chain_size ; ++i)
			{
				Rotate * r1x = rotates[2 * i];
				Rotate * r1z = rotates[2 * i + 1];
				Translate * t = translates[i];
				r1x->addSon(r1z);
				r1z->addSon(t);
				if (i < chain_size - 1) {
					Rotate * r2x = rotates[2 * i + 2];
					t->addSon(r2x);
				}


				Cylinder * tempx = new Cylinder(mat_articulation_x);
				Rotate * tempz = new Rotate(-M_PI / 2, Math::makeVector(0, 1, 0));
				tempz->addSon(new Cylinder(mat_articulation_z));
				Translate * articulation_x = new Translate(Math::makeVector(0.0, 0.0, -0.5));
				Translate * articulation_z = new Translate(Math::makeVector(0.5, 0.0, 0.0));
				articulation_x->addSon(tempx);
				articulation_z->addSon(tempz);

				Cylinder * segment = new Cylinder(mat_segment);
				Rotate * cylinder_tilted = new Rotate(-M_PI / 2, Math::makeVector(1, 0, 0)); // De base, le cylindre est orient� suivant l'axe z
				cylinder_tilted->addSon(segment);
				Scale * scale_Sphere = new Scale(Math::makeVector(1., 1., 1.) * articulation_size);
				Scale * scale_cylinder = new Scale(Math::makeVector(segment_width, segment_length, segment_width));
				r1z->addSon(scale_Sphere);
				scale_Sphere->addSon(articulation_x);
				scale_Sphere->addSon(articulation_z);
				r1z->addSon(scale_cylinder);
				scale_cylinder->addSon(cylinder_tilted);
			}
			m_root.addSon(rotates[0]);
		}

		virtual void initializeRendering()
		{
			drawFPS(true);
			m_camera.setPosition(Math::makeVector(6.46, 8.89, 12.24).normalized()*chain_length*2.6);// .normalized()*chain_length*1.5);
			Math::Matrix4x4f M = Math::Matrix4x4f(
				Math::makeVector(0.89, -0.27, 0.35, 0.0),
				Math::makeVector(0.0, 0.78, 0.62, 0.0),
				Math::makeVector(-0.44, -0.56, 0.70, 0.0),
				Math::makeVector(0, 0, 0, 1)
			);
			m_camera.setOrientation(M);

			T.normalize();
			T.scale(chain_length*0.8);

			dir = randomPointSphere();
			// Light
			HelperGl::Color lightColor(1.0, 1.0, 1.0);
			HelperGl::Color lightAmbiant(0.0, 0.0, 0.0, 0.0);
			Math::Vector4f lightPosition = Math::makeVector(0.0f, 0.0f, 10000.0f, 1.0f); // Point light centered in 0,0,0
			HelperGl::LightServer::Light * light = HelperGl::LightServer::getSingleton()->createLight(lightPosition.popBack(), lightColor, lightColor, lightColor);
			HelperGl::Material mat1 , mat2;
			mat1.setAmbiant(HelperGl::Color(1, 0, 1));
			mat2.setAmbiant(HelperGl::Color(0, 1, 1));

			light->enable();
			m_root.addSon(new SceneGraph::CoordinateSystem());
			addChain(chain_size, segment_length, segment_width, articulation_size);

			ccd = new CCD(&kchain, extremity);
			
			HelperGl::Material body;
			body.setDiffuse(HelperGl::Color(.5, .2, .2));
			body.setAmbiant(HelperGl::Color(0, 0, 0));
			HelperGl::Material wings;
			wings.setDiffuse(HelperGl::Color(1, 0.5, .5));
			wings.setAmbiant(HelperGl::Color(0, 0, 0));
			HelperGl::Material eyes;
			eyes.setDiffuse(HelperGl::Color(1, 0, 0));
			eyes.setAmbiant(HelperGl::Color(1, 0.1, 0.1));
			target_insect = new SceneGraph::Insect(body, wings, eyes, Math::makeVector(0, 0, 0), 0.0, 5, M_PI / 3, 0.1);
			target.addSon(target_insect);
			last_pos = target.getTranslation();

			float target_size = .1;
			target_sphere = new Scale(Math::makeVector(1.0, 1.0, 1.0)* target_size);
			target_sphere->addSon(new Sphere(mat1));
			target.addSon(target_sphere);
			Scale * target_scale_previous = new Scale(Math::makeVector(1.0, 1.0, 1.0)* target_size);
			target_scale_previous->addSon(new Sphere(mat2));
			target_previous.addSon(target_scale_previous);
			m_root.addSon(&target);
			//computeInterpolation();
			float step = 0.05;

			HelperGl::Material mat_blue, mat_grey;
			mat_blue.setAmbiant(HelperGl::Color(0.5, 0.5, 1.0));
			mat_grey.setAmbiant(HelperGl::Color(0.4, 0.4, 0.4));
			SceneGraph::NodeInterface * sphere = new SceneGraph::Sphere(mat_blue, 0.05);
			SceneGraph::NodeInterface * sphere2 = new SceneGraph::Sphere(mat_grey, 0.01);
			for (double i = 0; i < positions.size() - 1; i = i + step) {
				Math::Vector3f pos = T.compute(i, true);
				SceneGraph::Translate * t = new SceneGraph::Translate(pos);
				t->addSon(fmod(i, 1.0) < step ? sphere : sphere2);
				traj_tt.addSon(t);
			}
			m_root.addSon(&traj_tt);


			m_root.addSon(new SceneGraph::WireCube(chain_length,0.7,0.7,0.7));
		}

		Vector3f randomPointSphere(double radius = 1, bool inside = false) {
			double ksi1 = double(rand()) / RAND_MAX;
			double ksi2 = double(rand()) / RAND_MAX;
			double ksi3 = double(rand()) / RAND_MAX;
			double theta = 2 * M_PI * ksi1;
			double phi = acos(2 * ksi2 - 1);
			double r = inside ? pow(ksi3, 1. / 3.)*radius : radius;
			double x = r * sin(phi) * cos(theta);
			double y = r * cos(phi);
			double z = r * sin(phi) * sin(theta);
			return Math::makeVector(x, y, z);
		}



		void randomMoveTarget(double dt)
		{
			Vector3f random_direction = randomPointSphere()*dt * 5;
			target.setTranslation(target.getTranslation() + random_direction);
		}
		Vector3f bounceMoveTarget(double dt, double radius, double proba = 0.0, double speed = 7) {
			Vector3f pos = target.getTranslation();
			for (int i = 0; i < 3; ++i) {	
				if (pos[i] > radius || pos[i] < -radius) {
					dir[i] = -dir[i];
				}
			}
			Vector3f trans = target.getTranslation() + dir * dt*speed;
			target.setTranslation(trans);
			return trans;
		}

		std::vector<float> currentState()
		{
			std::vector<float> state(dofs.size());
			for (int i = 0; i < dofs.size(); ++i) state[i] = dofs[i];
			return state;
		}

		void lerp(double u, const std::vector<float> & previous, const std::vector<float> & next, std::vector<float> & interpoled)
		{
			if (interpoled.size() != previous.size()){
				interpoled = std::vector<float>(previous.size());
			}
			for (int i = 0; i < dofs.size(); ++i) interpoled[i] = (1 - u) * previous[i] + u * next[i];
		}

		void computeInterpolation() {
			resetChain();
			ccd->solve(target_previous.getTranslation(), range, max_variation*0.1, attenuation*0.1, eps*.1, 100000, false);
			previous = currentState();
			ccd->solve(target.getTranslation(), range, max_variation*0.1, attenuation*0.1, eps*0.1, 100000, false);
			next = currentState();
			updateChain(previous);
		}

		void background() {
			double r = state == 0 ? 1.0 : 0.95;
			double g = state == 1 ? 1.0 : 0.95;
			double b = state == 2 ? 1.0 : 0.95;
			glClearColor(r, g, b, 0.0);
		}
		Math::Vector3f speed;
		virtual void render(double dt)
		{
			background();
			GL::loadMatrix(m_camera.getInverseTransform());
			handleKeys();
			if (input) {
				last_input_time += dt;
			}
			time_interpolation = forward ? time_interpolation + dt : time_interpolation - dt;
			if ((forward && time_interpolation > max_time_interpolation) ||
				(!forward && time_interpolation < 0)) {
				forward = !forward;
			}
			u = time_interpolation / max_time_interpolation;
			time = fmod(time + dt,max_time);
			last_pos = target.getTranslation();
			target_insect->set_dt(dt);
			if (state == 0)
			{
				Vector3f pos = T.compute(time);	
				speed = pos - last_pos;
				target_insect->setOrientation(speed);
				target_insect->setWingSpeed(speed.norm() * 300);
				target.setTranslation(pos);
				ccd->solve(target.getTranslation(), range, max_variation, attenuation, eps, 200, reset);
				updateChain();
			}
			else if (state == 1) {
				Vector3f pos = bounceMoveTarget(dt, segment_length * chain_size, 0.3);
				speed = pos - last_pos;
				target_insect->setOrientation(speed);
				target_insect->setWingSpeed(speed.norm() * 300);
				ccd->solve(target.getTranslation(), range, max_variation, attenuation, eps, 200, reset);
				updateChain();
			}	
			else {
				lerp(u, previous, next, interpoled);
				updateChain(interpoled);
			}

			m_root.draw();
		}
	};
}

#endif