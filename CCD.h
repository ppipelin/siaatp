#pragma once

#include <Animation/KinematicChain.h>
#include <limits>
#include <algorithm>

typedef Animation::KinematicChain KChain;
typedef Animation::KinematicChain::Node KNode;
typedef Animation::KinematicChain::DegreeOfFreedom DOF;
typedef Math::Vector3f Vector3f;


class CCD
{
private:
	KChain * m_kchain;
	KNode * m_extremity;
	std::vector<DOF> m_dofs;

public:

	CCD(KChain * kchain, KNode * extremity) :
		m_kchain(kchain),
		m_extremity(extremity)
	{
		m_extremity->collectDegreesOfFreedom(m_dofs);
	}

	~CCD()
	{

	}

	bool solve(const Vector3f & P, float range, float max_variation, float attenuation, float epsilon, int cpt_max = 1000, bool reset = true)
	{
		if (reset) {
			for (int i = 0; i < m_dofs.size(); ++i)
				m_dofs[i] = 0;
		}
		float m = std::numeric_limits<float>::max();
		Vector3f off = Math::makeVector(m, m, m);
		int cpt = cpt_max;
		while (off.norm() > range && cpt > 0) {
			off = offset(P);
			convergeTowardOffset(off, range, max_variation, attenuation, epsilon);
			--cpt;
		}
		return off.norm() < range;
	}

	Vector3f extremity()
	{
		Math::Matrix4x4f to_global = m_extremity->getGlobalTransformation();
		return to_global * Math::makeVector(0, 0, 0);
	}

	Vector3f offset(const Vector3f & P)
	{
		return P - extremity();
	}

	void convergeTowardOffset(const Vector3f & offset, float range, float max_variation, float attenuation, float epsilon)
	{
		for (int i = 0; i < m_dofs.size(); ++i)
		{
			Vector3f Jinv = m_kchain->derivate(m_extremity, offset, m_dofs[i], epsilon).inv();
			float delta = (offset * Jinv)*attenuation;
			if (delta > max_variation)
				delta = max_variation;
			if (delta < -max_variation)
				delta = -max_variation;
			m_dofs[i] = m_dofs[i] + delta;
		}
	}

	void convergeToward(const Vector3f & P, float range, float max_variation, float attenuation, float epsilon)
	{
		Vector3f off = offset(P);
		if (off.norm() > range) {
			convergeTowardOffset(off, range, max_variation, attenuation, epsilon);
		}
	}
	
	
};