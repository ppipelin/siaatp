#ifndef _Application_TP1_siaa_H
#define _Application_TP1_siaa_H

#include <HelperGl/Camera.h>
#include <HelperGl/LightServer.h>
#include <HelperGl/Color.h>
#include <Application/BaseWithKeyboard.h>
#include <Application/KeyboardStatus.h>
#include <SceneGraph/NodeInterface.h>
#include <SceneGraph/Group.h>
#include <SceneGraph/Sphere.h>
#include <SceneGraph/MeshVBO_v2.h>
#include <SceneGraph/Cylinder.h>
#include <SceneGraph/Insect.h>
#include <SceneGraph/Translate.h>
#include <SceneGraph/Rotate.h>
#include <SceneGraph/Scale.h>
#include <SceneGraph/Insect.h>
#include <GL/compatibility.h>
#include <HermiteSplines.h>
#include <TemporalTrajectory.h>

namespace Application
{
	class TP1_siaa : public BaseWithKeyboard
	{
	protected:
		HelperGl::Camera m_camera ;
		SceneGraph::Insect * insect;
		SceneGraph::Translate * position;
		SceneGraph::Group m_root, traj_hs, traj_tt;
		Math::Vector3f P0 = Math::makeVector(0, 0, 0);
		Math::Vector3f D0 = Math::makeVector(0, 0, 0);
		Math::Vector3f P1 = Math::makeVector(3, 3, 3);
		Math::Vector3f D1 = Math::makeVector(0, 20, 0);
		Math::Vector3f last_pos = Math::makeVector(0, 0, 0);
		HermiteSplines hermitpline = HermiteSplines(P0, P1, D0, D1);

		SceneGraph::Translate vect;
		
		// Temps total de l'animation :
		
		std::vector<Math::Vector3f> positions = { 
			Math::makeVector(0.0,0.0,0.),
			Math::makeVector(1.,1.,1.),
			Math::makeVector(1.,1.,-1.),
			Math::makeVector(-1.,1.,-1.),
			Math::makeVector(-2.,0.0,1.0),
			Math::makeVector(0.0,4.0,4.0),
			Math::makeVector(2.0,1.0,2.0),
			Math::makeVector(1.0,-1.0,2.0),
			Math::makeVector(-1.0,-3.0,1.0),
			Math::makeVector(-2.0,-1.0,-2.0),
			Math::makeVector(0.0,1.0,-2.0),
			Math::makeVector(0.0,0.0,0.0)
		};
		
		TemporalTrajectory temptraj = TemporalTrajectory(positions, true);
		double time = 0;
		
		double max_time = temptraj.duration();

		virtual void handleKeys() 
		{
			// The camera translation speed
			float cameraSpeed = 5.0f ;
			// The camera rotation speed (currently not used)
			float rotationSpeed = float(Math::pi/2.0) ;

			// quit
			//if(m_keyboard.isPressed('q')) { quit() ; }
			// Go front
			
			if (m_keyboard.isPressed('z')) { m_camera.translateFront(-cameraSpeed * (float)getDt()); }
			// Go back
			else if(m_keyboard.isPressed('s')) { m_camera.translateFront(cameraSpeed * (float)getDt()); }
			// Go left
			else if(m_keyboard.isPressed('q')) { m_camera.translateRight(-cameraSpeed * (float)getDt()); }
			// Go right
			else if (m_keyboard.isPressed('d')) { m_camera.translateRight(cameraSpeed * (float)getDt()); }
			// Go down
			
			if (m_keyboard.isPressed('w')) {m_camera.translateUp(-cameraSpeed * (float)getDt());}
			// Go up
			else if(m_keyboard.isPressed(VK_SPACE)) { m_camera.translateUp(cameraSpeed * (float)getDt()); }

			if (m_keyboard.isPressed('5')) {m_camera.rotateUp(rotationSpeed* (float)getDt());}
			else if (m_keyboard.isPressed('2')) { m_camera.rotateUp(-rotationSpeed* (float)getDt()); }
			else if (m_keyboard.isPressed('3')) { m_camera.rotateRight(rotationSpeed * (float)getDt()); }
			else if (m_keyboard.isPressed('1')) { m_camera.rotateRight(-rotationSpeed * (float)getDt()); }
		}

	public:
		TP1_siaa()
		{
		}
		
		virtual void initializeRendering()
		{
			drawFPS(true);

			m_camera.setPosition(Math::makeVector(-4., 2.46, 4.28));
			Math::Matrix4x4f M = Math::Matrix4x4f(
				Math::makeVector(0.436, 0.127, -0.89, 0.0),
				Math::makeVector(0.0, 0.99, 0.141, 0.0),
				Math::makeVector(0.99, -0.0616, 0.432, 0.0),
				Math::makeVector(0, 0, 0, 1)
			);
			m_camera.setOrientation(M);
			// Light
			HelperGl::Color lightColor(1.0,1.0,1.0);
			HelperGl::Color lightAmbiant(0.0,0.0,0.0,0.0);
			Math::Vector4f lightPosition = Math::makeVector(0.0f,0.0f,10000.0f,1.0f) ; // Point light centered in 0,0,0
			HelperGl::LightServer::Light * light = HelperGl::LightServer::getSingleton()->createLight(lightPosition.popBack(), lightColor, lightColor, lightColor) ;
			HelperGl::Material mat;
			mat.setAmbiant(HelperGl::Color(0.4, 0.4, 0.4));
			HelperGl::Material mat2;
			mat2.setAmbiant(HelperGl::Color(0.5, 0.5, 1.0));

			HelperGl::Material body;
			body.setDiffuse(HelperGl::Color(.5, .2, .2));
			body.setAmbiant(HelperGl::Color(0, 0, 0));
			HelperGl::Material wings;
			wings.setDiffuse(HelperGl::Color(1, 0.5, .5));
			wings.setAmbiant(HelperGl::Color(0, 0, 0));
			HelperGl::Material eyes;
			eyes.setDiffuse(HelperGl::Color(1, 0, 0));
			eyes.setAmbiant(HelperGl::Color(1,0.1, 0.1));

			light->enable();
			SceneGraph::NodeInterface * pt = new SceneGraph::Sphere(mat);

			Math::Vector3f pos = Math::makeVector(0, 0, 0);

			// Parametres de l'insectes
			double a = 0.0; // Angle de depart
			double ws = 5.0; // 5 battements par seconde
			double amplitude = M_PI / 3; // Amplitude totale du battement d'aile en radian
			double s = 0.05; // Facteur de taille

			insect = new SceneGraph::Insect(body, wings, eyes, pos, a, ws, amplitude, s);
			last_pos = pos;
			m_root.addSon(insect);
			m_root.addSon(&vect);
			SceneGraph::NodeInterface * sphere = new SceneGraph::Sphere(mat2,0.05);
			SceneGraph::NodeInterface * sphere2 = new SceneGraph::Sphere(mat, 0.01);	
			//m_root.addSon(sphere2);
			//vect.addSon(sphere);
			float step = 0.05;
			for (double i = 0; i < positions.size()-1; i = i + step) {
				Math::Vector3f pos = temptraj.compute(i, true);
				SceneGraph::Translate * t = new SceneGraph::Translate(pos);
				t->addSon(fmod(i,1.0)< step ? sphere : sphere2);
				traj_tt.addSon(t);
			}
			m_root.addSon(&traj_tt);
			m_root.addSon(new SceneGraph::CoordinateSystem());
			
		}


		void background() {
			glClearColor(0.8, 0.8, 0.8, 0.0);
		}

		virtual void render(double dt)
		{
			background();
			handleKeys() ;
			GL::loadMatrix(m_camera.getInverseTransform()) ;

			insect->set_dt(dt);

			time = fmod(time + dt, max_time);		

			Math::Vector3f new_pos = temptraj.compute(time);
	
			Math::Vector3f speed = new_pos - last_pos;
			vect.setTranslation(speed.normalized());
			last_pos = new_pos;
			insect->setWingSpeed(speed.norm() * 500);
			insect->setPosition(new_pos);
			insect->setOrientation(speed);
			m_root.draw() ;
		}
	};
}

#endif